# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-05 07:02
from __future__ import unicode_literals

from django.db import migrations


def migrer_id_artiste(apps, schema):
    Morceau = apps.get_model('musiques', 'Morceau')
    Artiste = apps.get_model('musiques', 'Artiste')
    artiste_list = [fields['artiste']
                    for fields in Morceau.objects.all().values('artiste')]
    for m in Morceau.objects.all():
        a = Artiste.objects.get(nom=m.artiste)
        m.id_artiste = a
        m.save()    



def annuler_migrer_id_artiste(apps, schema):

    Morceau = apps.get_model('musiques', 'Morceau')

    for m in Morceau.objects.all():
        m.id_artiste = NULL
        m.save()


class Migration(migrations.Migration):

    dependencies = [
        ('musiques', '0005_morceau_id_artiste'),
    ]

    operations = [
        migrations.RunPython(
            migrer_id_artiste, reverse_code=annuler_migrer_id_artiste)

    ]

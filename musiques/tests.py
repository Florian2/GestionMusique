from django.test import TestCase

# Create your tests here.
from django.urls import reverse
from django.urls.exceptions import NoReverseMatch
from django.core.urlresolvers import resolve
from selenium.webdriver.support.ui import Select

from musiques.models import Morceau, Artiste

from selenium import webdriver
import time


class MorceauTestCase(TestCase):
    def setUp(self):
        a1 = Artiste.objects.create(nom="artiste1",prenom="pre_art", date_naiss="1996-12-28")
        Morceau.objects.create(titre='Musique1',artiste=a1)
        Morceau.objects.create(titre='Musique2',artiste=a1)
        Morceau.objects.create(titre='Musique3',artiste=a1)
    def test_morceau_url_name(self):
        try:
            url = reverse('musiques:morceau_detail',args=[1])
        except NoReverseMatch:
            assert False
    def test_morceau_url(self):
        morceau = Morceau.objects.get(titre='Musique1')
        url = reverse('musiques:morceau_detail',args=[morceau.pk])
        response = self.client.get(url)
        assert response.status_code == 200

    def test_home_page(self):
        found = resolve('/musiques/')
        self.assertEqual(found.view_name,'musiques:morceau_list')


class SeleniumTestCase(TestCase):
    def setUp(self):
        self.mydriver = webdriver.Chrome()

    def test_create_delete(self):
        # Creation d'un artiste NomTest
        baseurl = "http://localhost:8000/musiques/Ajout_artiste"
        self.mydriver.get(baseurl)
        time.sleep(9)
        nom = self.mydriver.find_element_by_id('id_nom')
        nom.clear()
        nom.send_keys('NomTest')
        prenom = self.mydriver.find_element_by_id('id_prenom')
        prenom.clear()
        prenom.send_keys('prenomTest')
        date_naiss = self.mydriver.find_element_by_id('id_date_naiss')
        date_naiss.clear()
        date_naiss.send_keys('1996-12-28')
        self.mydriver.find_element_by_css_selector("input[type='submit']").submit()


        # Création d'un morceau testMorceau
        baseurl = "http://localhost:8000/musiques/Ajout_morceau"
        self.mydriver.get(baseurl)
        artiste = Select(self.mydriver.find_element_by_id("id_artiste"))
        artiste.select_by_index(3)# selection de l'artiste numero 2 dans les option s
        titre = self.mydriver.find_element_by_id('id_titre')
        titre.clear()
        titre.send_keys('testMorceau')
        date = self.mydriver.find_element_by_id('id_date_sortie')
        date.clear()
        date.send_keys('01/01/2002')
        album = self.mydriver.find_element_by_id('id_album')
        album.clear()
        album.send_keys('testAlbum')
        genre = self.mydriver.find_element_by_id('id_genre')
        genre.clear()
        genre.send_keys('testGenre')
        parole = self.mydriver.find_element_by_id('id_parole')
        parole.clear()
        parole.send_keys('testParole')
        self.mydriver.find_element_by_css_selector("input[type='submit']").submit()

        # Partie suppression
        # Supression d'un morceau
        baseurl = "http://localhost:8000{chemin}".format(chemin=reverse('musiques:morceau_list'))
        self.mydriver.get(baseurl)
        self.mydriver.find_element_by_id('testMorceau').click()
        time.sleep(3)

        self.mydriver.find_element_by_id('testMorceau-delete').click()
        self.mydriver.find_element_by_css_selector("input[type='submit']").submit()

        baseurl = "http://localhost:8000{chemin}".format(chemin=reverse('musiques:artiste_list'))
        self.mydriver.get(baseurl)
        self.mydriver.find_element_by_id('NomTest-delete').click()
        self.mydriver.find_element_by_css_selector("input[type='submit']").submit()

        self.mydriver.close()

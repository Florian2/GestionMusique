from django.shortcuts import render

from django.http import HttpResponse


def home(request):
    return HttpResponse("Hello, world. You're at the polls index.")
# def morceau_detail(request,pk):
#     return HttpResponse("morceau_detail")


from django.views.generic.list import ListView
from django.views.generic import DetailView, UpdateView, CreateView, DeleteView
from .models import Morceau, Artiste
from django.urls import reverse_lazy


class MorceauDetailView(DetailView):
    model = Morceau
    context_object_name = "Morceau"


class MorceauList(ListView):
    model = Morceau
    context_object_name = 'Morceau'


class UpdateMorceauView(UpdateView):
    model = Morceau
    fields = ['titre', 'artiste','album', 'date_sortie','genre','parole']


class MorceauAjoutView(CreateView):
    model = Morceau
    fields = ['titre', 'artiste','album', 'date_sortie','genre','parole']


class MorceauDeleteView(DeleteView):
    model = Morceau
    success_url = reverse_lazy('musiques:morceau_list')

class ArtisteDetailView(DetailView):
    model = Artiste
    context_object_name = "Artiste"


class ArtisteList(ListView):
    model = Artiste
    context_object_name = 'Artiste'


class UpdateArtisteView(UpdateView):
    model = Artiste
    fields = ['nom', 'prenom', 'date_naiss']


class ArtisteAjoutView(CreateView):
    model = Artiste
    fields = ['nom', 'prenom', 'date_naiss']


class ArtisteDeleteView(DeleteView):
    model = Artiste
    success_url = reverse_lazy('musiques:artiste_list')

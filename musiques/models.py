from django.db import models
from django.urls import reverse

class Morceau(models.Model):
    artiste = models.ForeignKey('Artiste',null=False,on_delete=models.CASCADE)
    titre = models.CharField(max_length=64)
    date_sortie = models.DateField(null=True)
    album = models.CharField(null=True,max_length=64)
    genre = models.CharField(null=True,max_length=64)
    parole =models.CharField(null=True,max_length=2000)


    def __str__(self):
        return '{self.titre} ({self.artiste.nom})'.format(self=self)

    def get_absolute_url(self):
        return reverse('musiques:morceau_detail', args=[str(self.id)])

class Artiste(models.Model):
    nom = models.CharField(max_length=64)
    prenom = models.CharField(null=True,max_length=64)
    date_naiss = models.DateField(null=True)

    def __str__(self):
        return '{self.nom} {self.prenom}'.format(self=self)

    def get_absolute_url(self):
        return reverse('musiques:artiste_detail', args=[str(self.id)])

from django.conf.urls import url
from .views import home, MorceauDetailView, MorceauList, MorceauAjoutView, UpdateMorceauView, MorceauDeleteView, ArtisteDetailView, ArtisteList, ArtisteAjoutView, UpdateArtisteView, ArtisteDeleteView


app_name = 'musiques'
urlpatterns = [
    url(r'^home/$', home, name='home'),
    url(r'^(?P<pk>\d+)$', MorceauDetailView.as_view(), name='morceau_detail'),
    url(r'^$', MorceauList.as_view(), name='morceau_list'),
    url(r'^Ajout_morceau/', MorceauAjoutView.as_view(), name='morceau_ajout'),
    url(r'^Update_morceau/(?P<pk>\d+)$',
        UpdateMorceauView.as_view(), name='morceau_update'),
    url(r'^Delete_morceau/(?P<pk>\d+)$',
    MorceauDeleteView.as_view(), name='morceau_delete'),
    url(r'^artiste/(?P<pk>\d+)$', ArtisteDetailView.as_view(), name='artiste_detail'),
    url(r'^artiste', ArtisteList.as_view(), name='artiste_list'),
    url(r'^Ajout_artiste/', ArtisteAjoutView.as_view(), name='artiste_ajout'),
    url(r'^Update_artiste/(?P<pk>\d+)$',
        UpdateArtisteView.as_view(), name='artiste_update'),
    url(r'^Delete_artiste/(?P<pk>\d+)$',
        ArtisteDeleteView.as_view(), name='artiste_delete'),
]

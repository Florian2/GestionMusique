from django.contrib import admin

# Register your models here.

from musiques.models import Morceau,Artiste

class MorceauAdmin(admin.ModelAdmin):
     list_display=('titre','artiste')
admin.site.register(Morceau,MorceauAdmin)


class ArtisteAdmin(admin.ModelAdmin):
     list_display=('nom',)



admin.site.register(Artiste,ArtisteAdmin)